<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * INSEE city: code database
 */

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (__DIR__.'/../../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

// ============================================================
class syntax_plugin_ol3 extends DokuWiki_Syntax_Plugin {
 
    // ============================================================
    function getType () { return 'substition'; }
    function getPType () { return 'block'; }
    function getSort () { return 299; }
    function connectTo ($mode) {
        $this->Lexer->addSpecialPattern ('\{\{ol3[^}]*\}\}', $mode, 'plugin_ol3');
    }

    // ============================================================
    function handle ($match, $state, $pos, Doku_Handler $handler) {
        switch ($state) {
        case DOKU_LEXER_SPECIAL :
            return array ($state, trim (substr ($match, 5, -2))); // "{{ol3" => 5 "}}" => 2
        }
        return false;
    }

    // ============================================================
    function render ($mode, Doku_Renderer $renderer, $indata) {
        $dumy = "";
        if (empty($indata))
            return false;
        if ($mode != 'xhtml')
            return false;
        list ($instr, $data) = $indata;
        switch ($instr) {
        case DOKU_LEXER_SPECIAL :
            $args = " ".$data." ";
            if (preg_match_all ("#(\"[^\"]*\")* help (\"[^\"]*\")*#", strtolower ($args), $dumy) > 0) {
                $renderer->doc .= $this->ol3Help ();
                return true;
            }
            if (preg_match_all ("#(\"[^\"]*\")* test (\"[^\"]*\")*#", strtolower ($args), $dumy) > 0) {
                $renderer->doc .= $this->ol3Test ();
                return true;
            }
        }
        return true;
    }

    // ============================================================
    function ol3Help () {
        $url = "http://admin.parlenet.org/plugins/ol3/";
        return
            '<h1>Help OL3</h1>'.NL.
            '<ul>'.NL.
            ' <li><b>{{ol3</b> help <b>}}</b></li>'.NL.
            ' <li><b>{{ol3</b> test <b>}}</b></li>'.NL.
            '</ul>'.NL.
            '<p><a class="urlextern" rel="nofollow" title="'.$url.'" href="'.$url.'">'.$url.'</a></p>'.NL;
    }

    // ============================================================
    function ol3Test () {
        return
            '<script type="text/javascript" src="/lib/plugins/ol3/test.js" defer="defer" charset="utf-8"></script>'.NL.
            '<div id="map" class="map" style="width:200px;height:200px;"></div>'.NL;
    }

    // ============================================================
}
