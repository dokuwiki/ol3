jQuery (function () {
    var layer = new ol.layer.Tile ({
	source: new ol.source.OSM ()
    });
    var view = new ol.View ({
	center: ol.proj.transform ([-2.54833, 47.95583], "EPSG:4326", "EPSG:3857"),
	zoom: 6
    });
    var map = new ol.Map ({
	target: "map",
	layers: [layer],
	view: view
    });
});
